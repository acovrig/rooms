# About
This is an interface that parses the course schedule on Southern Adventist University (southern.edu) and generates a calendar of when classrooms are available.

This project was created by Austin Covrig for Prog. Scripting Admin in F15

# Running
1. Install ```docker``` ([linux](https://docs.docker.com/linux/step_one), [mac](https://docs.docker.com/mac/step_one/), or [windows](https://docs.docker.com/windows/step_one/))
2. Install ```docker-compose``` ([all platforms](https://docs.docker.com/compose/install/))
This project can be run by doing the following:
 - On linux, it is easiest/best to add your user to the ```docker``` group and logout/login, otherwise, you will need to run ```sudo docker``` instead of just ```docker``` below.
```
git clone https://acovrig@bitbucket.org/acovrig/rooms.git rooms
cd rooms
docker-compose up -d
```
## OR (without compose)
```
docker run -itd --name rooms_sql --restart=always mysql
docker run -itd --name rooms --restart=always -p 80 -e RAILS_SERVE_STATIC_FILES=true --link rooms_sql:db acovrig/rooms
```
This will spin up the project in a docker container. The port the container gets assigned will be displayed on run.

# Deploying
The easiest way to expose this publicly is to use an ```nginx``` *reverse proxy*. I have included a sample nginx config (```/etc/nginx/sites-enabled/default```) below for ease of deployment:
```nginx
upstream rooms {
  # This port number is provided by ```docker-compose ps``` or ```docker ps```.
  server 127.0.0.1:32770;
}

server { # rooms
  server_name hw.cs.southern.edu;
  location /rooms/ {
    proxy_pass http://rooms/;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Port $server_port;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_read_timeout 900s;
    proxy_ssl_session_reuse off;
  }
}
```
This also allows for serving over **HTTPS** instead of *HTTP*:
```nginx
upstream rooms {
  # This port number is provided by ```docker-compose ps``` or ```docker ps```.
  server 127.0.0.1:32770;
}

server { # rooms HTTPS
  listen 443 ssl;
  server_name hw.cs.southern.edu;

  ssl_certificate           /path/to/ssl/cert;
  ssl_certificate_key       /path/to/ssl/private/key;
  ssl_trusted_certificate   /path/to/ssl/full/chain/if/needed;

  ssl_session_cache  builtin:1000  shared:SSL:10m;
  ssl_protocols  TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4;
  ssl_prefer_server_ciphers on;
  proxy_ssl_session_reuse off;

  location /rooms/ {
    proxy_pass http://rooms/;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Port $server_port;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_read_timeout 900s;
    proxy_ssl_session_reuse off;
  }
}
```

# Screenshots

## Downloading
![downloading.PNG](https://bitbucket.org/repo/bXqnLB/images/3768187929-downloading.PNG)

## Running
![done.PNG](https://bitbucket.org/repo/bXqnLB/images/2032452172-done.PNG)

## Started
![Screen Shot 2015-12-21 at 1.39.52 PM.png](https://bitbucket.org/repo/bXqnLB/images/4028764002-Screen%20Shot%202015-12-21%20at%201.39.52%20PM.png)