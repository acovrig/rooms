class CreateClassRooms < ActiveRecord::Migration
  def change
    create_table :class_rooms do |t|
      t.string :semester
      t.string :class_code
      t.string :class_name
      t.string :instructor
      t.string :location
      t.string :day
      t.string :class_start_time
      t.string :class_end_time

      t.timestamps null: false
    end
  end
end
