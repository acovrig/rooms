class HomeController < ApplicationController
  def index
    render 'index' and return if @current == 'error'
    redirect_to semester_path(@current)
  end

  def view
    @classes = []
    ClassRoom.where(semester: params[:semester], location: "#{params[:building]} #{params[:room]}").each do |class_room|
      class_room_info = {}
      class_room_info[:code] = class_room.class_code
      class_room_info[:name] = class_room.class_name
      class_room_info[:instructor] = class_room.instructor
      class_room_info[:start] = class_room.class_start_time
      class_room_info[:end] = class_room.class_end_time
      class_room_info[:days] = []

      next if class_room.class_start_time.empty? or class_room.class_end_time.empty?
      start_time = Time.parse(class_room.class_start_time)
      end_time = Time.parse(class_room.class_end_time)
      class_length = (end_time - start_time)

      if class_room.class_start_time.include? ':00'
        class_room_info[:style] = 'margin-top: 0%; '
      elsif class_room.class_start_time.include? ':10'
        class_room_info[:style] = 'margin-top: 0.8%; '
      elsif class_room.class_start_time.include? ':15'
        class_room_info[:style] = 'margin-top: 1%; '
      elsif class_room.class_start_time.include? ':20'
        class_room_info[:style] = 'margin-top: 1.5%; '
      elsif class_room.class_start_time.include? ':30'
        class_room_info[:style] = 'margin-top: 2%; '
      elsif class_room.class_start_time.include? ':45'
        class_room_info[:style] = 'margin-top: 4%; '
			else
				p "ERROR: unknown time #{class_room.class_start_time}"
      end
      class_room_info[:style] += "height: #{class_length/500}%;"

      days = class_room.day
      if days.include? 'M'
        days.sub!(/M/, '')
        class_room_info[:days] << 'Monday'
      end
      if days.include? 'W'
        days.sub!(/W/, '')
        class_room_info[:days] << 'Wednesday'
      end
      if days.include? 'Th'
        days.sub!(/Th/, '')
        class_room_info[:days] << 'Thursday'
      end
      if days.include? 'F'
        days.sub!(/F/, '')
        class_room_info[:days] << 'Friday'
      end
      if days.include? 'T'
        days.sub!(/T/, '')
        class_room_info[:days] << 'Tuesday'
      end
      @classes << class_room_info
    end

    @days = %w(Monday Tuesday Wednesday Thursday Friday)
  end
end
