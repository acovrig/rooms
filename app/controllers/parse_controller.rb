require 'open-uri'
class ParseController < ApplicationController
  def parse
    excluded_locations = ['Arranged', 'OFF CAMPUS', 'Online']
    page = Nokogiri::HTML(open("https://myaccess.southern.edu/apps/courseschedule/Default.aspx?Term=#{params[:semester]}"))
    num_pages = page.xpath('//div[@id="ctl00_ContentPlaceHolder1_GridView1_ctl01_pagerArea"]//a').count + 1
    (1..num_pages).each do |page_num|
      puts "Parsing page #{page_num}/#{num_pages}: "
      page = Nokogiri::HTML(open("https://myaccess.southern.edu/apps/courseschedule/Default.aspx?Page=#{page_num}&Term=#{params[:semester]}")) if page_num > 1
      data = []
      page.xpath('//table[@id="ctl00_ContentPlaceHolder1_GridView1"]/tr').each do |row|
        info = {}
        column=row.xpath('td')
        next if column.count < 5
        info[:semester] = params[:semester]
        info[:class_code] = column[1].xpath('div[contains(@style, "white-space: nowrap")]').text
        info[:class_name] = column[2].xpath('.//a').text
        info[:instructor] = column.xpath(".//span[substring(@id, string-length(@id) - string-length('Label6') +1) = 'Label6']").text
        info[:location] = column.xpath(".//span[substring(@id, string-length(@id) - string-length('Label11') +1) = 'Label11']").text
        next if excluded_locations.include? info[:location]
        info[:day] = column.xpath(".//span[substring(@id, string-length(@id) - string-length('Label14') +1) = 'Label14']").text
        info[:class_start_time] = column.xpath(".//span[substring(@id, string-length(@id) - string-length('Label15') +1) = 'Label15']").text.gsub(/ /, '')
        info[:class_end_time] = column.xpath(".//span[substring(@id, string-length(@id) - string-length('Label16') +1) = 'Label16']").text.gsub(/ /, '')

        ClassRoom.where(info).first_or_create(info)

      end
    end
    redirect_to root_path
  end
end
