class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action do
    @semesters = ClassRoom.all.collect{|classroom| classroom.semester}.uniq.sort
    locations = ClassRoom.where("semester=?", params[:semester]).collect{|classroom| classroom.location}
    buildings = []
		@rooms = []
    @buildings = []
    locations.each do |classroom|
			loc=classroom.split(' ')
			buildings << loc[0]
			@rooms << loc[1] if params[:building] == loc[0]
		end
		buildings.sort!.uniq!
    buildings.each do |building|
      name = Building.find_by(code: building)
      if name.nil?
        @buildings << {name: '', code: building}
      else
        @buildings << {name: name.name, code: building}
      end
    end
    p @buildings
		@rooms.sort!.uniq!

    year=Time.now.year.to_s[2..3]
    if (Date.parse("#{Time.now.year.to_s}-1-1")..Date.parse("#{Time.now.year.to_s}-5-15")).cover? Time.now
      @current = "W#{year}"
    elsif (Date.parse("#{Time.now.year.to_s}-8-1")..Date.parse("#{Time.now.year.to_s}-12-20")).cover? Time.now
      @current = "F#{year}"
    else
      @current = 'error'
    end
  end
end
